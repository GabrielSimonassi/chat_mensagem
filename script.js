var containerId = 0;

function createText(){
  let input = document.getElementById('message');
  let texto = document.createElement('p');
  texto.innerText = input.value;
  let message = document.createElement('div');
  message.classList.add('oldMessage');
  message.append(texto);
  input.value = '';

  return (message);
}

function createButton(){
  let excludeButton = document.createElement('button');
  excludeButton.classList.add('excludeButton');
  excludeButton.setAttribute('Id', containerId);
  if (excludeButton){
    excludeButton.addEventListener('click', deleteMessage);
  }
  excludeButton.innerText = "excluir";

  return (excludeButton);
}

function createContainer(message, button){
  let messageContainer = document.createElement('div');
  messageContainer.classList.add('container')
  messageContainer.setAttribute('Id', `container${containerId}`);
  messageContainer.append(message);
  messageContainer.append(button);
  containerId++;

  return (messageContainer);
}

function deleteMessage(event){
  let lastMessage = document.getElementById(`container${event.target.id}`);
  lastMessage.remove();
}

function checkMessage(){

}

function sendMessage(){
  let input = document.getElementById('message');
  let text = input.value.trim();
  if(!text.length) {
    input.value = "";
    return ;
  };
  let message = createContainer(createText(), createButton());
  let inbox = document.querySelector('.messageHistory');
  inbox.append(message);
}

let sendBtn = document.querySelector('.sendBtn');
sendBtn.addEventListener('click', sendMessage);